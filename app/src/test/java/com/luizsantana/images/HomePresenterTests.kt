package com.luizsantana.images

import com.luizsantana.images.model.Image
import com.luizsantana.images.model.ResponseDTO
import com.luizsantana.images.mvp.home.HomeContract
import com.luizsantana.images.mvp.home.HomePresenter
import com.luizsantana.images.services.GettyService
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import rx.Observable
import rx.Scheduler
import rx.android.plugins.RxAndroidPlugins
import rx.android.plugins.RxAndroidSchedulersHook
import rx.plugins.RxJavaPlugins
import rx.plugins.RxJavaSchedulersHook
import rx.schedulers.Schedulers

class HomePresenterTests {

    val mockImage = Image(id = "8987987",
            title = "This is a cute dog mockImage",
            caption = "This is the cute dog mockImage caption describing the content",
            displaySizes = listOf(Image.Data(name = "Image name", uri = "https://images-na.ssl-images-amazon.com/images/G/01/img15/pet-products/small-tiles/23695_pets_vertical_store_dogs_small_tile_8._CB312176604_.jpg")))

    @Before
    @Throws(Exception::class)
    fun setUp() {
        RxAndroidPlugins.getInstance().registerSchedulersHook(object : RxAndroidSchedulersHook() {
            override fun getMainThreadScheduler(): Scheduler {
                return Schedulers.immediate()
            }
        })
        RxJavaPlugins.getInstance().registerSchedulersHook(object  : RxJavaSchedulersHook() {
            override fun getIOScheduler(): Scheduler {
                return Schedulers.immediate()
            }
        })
    }

    @After
    fun tearDown() {
        RxAndroidPlugins.getInstance().reset()
        RxJavaPlugins.getInstance().reset()
    }

    @Test
    fun loadFirstPageShouldAddDataToView() {
        val mockView = mock<HomeContract.View>()
        val gettyService = mock<GettyService>()

        Mockito.`when`(gettyService.load(1)).thenReturn(Observable.create {
            val response = ResponseDTO(1, listOf(mockImage))
            it.onNext(response)
            it.onCompleted()
        })


        val presenter = HomePresenter(mockView, gettyService)
        presenter.load(page = 1)

        verify(mockView, times(1)).addListOfResults(listOf(mockImage))
    }

    @Test
    fun emptyResultShouldShowEmptyMessage() {
        val mockView = mock<HomeContract.View>()
        val gettyService = mock<GettyService>()

        Mockito.`when`(gettyService.load(1)).thenReturn(Observable.create {
            val response = ResponseDTO<Image>(0, emptyList())
            it.onNext(response)
            it.onCompleted()
        })


        val presenter = HomePresenter(mockView, gettyService)
        presenter.load(page = 1)

        verify(mockView, times(1)).showEmptyResultMessage()
    }

    @Test
    fun anyErrorWhileLoadingImagesShouldShowMessage() {
        val mockView = mock<HomeContract.View>()
        val gettyService = mock<GettyService>()

        Mockito.`when`(gettyService.load(1)).thenReturn(Observable.create {
            it.onError(RuntimeException("Message"))
        })


        val presenter = HomePresenter(mockView, gettyService)
        presenter.load(page = 1)

        verify(mockView, times(1)).showError()
    }
}