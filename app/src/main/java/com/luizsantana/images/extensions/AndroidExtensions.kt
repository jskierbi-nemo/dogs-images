@file:JvmName("AndroidUtils")
package com.luizsantana.images.extensions

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.luizsantana.images.R
import jp.wasabeef.glide.transformations.CropCircleTransformation
import java.text.SimpleDateFormat
import java.util.*

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun ImageView.load(url: String, circular: Boolean = false) {
    val request = Glide.with(context)
            .load(url)
            .placeholder(R.drawable.app_icon)

    if (circular) {
        request.bitmapTransform(CropCircleTransformation(context))
    }

    request.into(this)
}

fun Date.format(pattern: String = "dd/MM/yyyy"): String {
    return SimpleDateFormat(pattern).format(this)
}

fun String.toDate(pattern: String = "yyyy MM dd"): Date {
    return SimpleDateFormat(pattern).parse(this)
}
