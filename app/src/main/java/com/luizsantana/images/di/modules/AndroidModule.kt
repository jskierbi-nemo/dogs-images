package com.luizsantana.images.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AndroidModule(val context: Context) {
    @Provides fun providesActivity(): Context {
        return context
    }
}