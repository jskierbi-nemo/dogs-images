package com.luizsantana.images.di.components

import android.content.Context
import com.luizsantana.images.di.modules.AndroidModule
import com.luizsantana.images.di.modules.NetworkModule
import dagger.Component
import retrofit2.Retrofit

@Component(modules = arrayOf(NetworkModule::class, AndroidModule::class))
interface ApplicatonComponent {
    fun retrofit(): Retrofit
    fun context(): Context
}