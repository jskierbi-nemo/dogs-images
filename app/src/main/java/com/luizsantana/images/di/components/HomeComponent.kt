package com.luizsantana.images.di.components

import com.luizsantana.images.activity.HomeActivity
import com.luizsantana.images.di.modules.HomeModule
import dagger.Component

@Component(modules = arrayOf(HomeModule::class), dependencies = arrayOf(ApplicatonComponent::class))
interface HomeComponent {
    fun inject(homeActivity: HomeActivity)
}