package com.luizsantana.images.di.modules

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.luizsantana.images.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule(val application : Application) {

    @Provides fun provideOkHttpCache(): Cache {
        val cacheSize = 10 * 1024 * 1024L // 10 MiB
        val cache = Cache(application.getCacheDir(), cacheSize)
        return cache
    }

    @Provides fun provideGson(): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
    }

    @Provides fun provideOkHttpClient(cache: Cache): OkHttpClient {
        val clientBuilder = OkHttpClient().newBuilder()
                .cache(cache)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor {
                    val original = it.request()

                    val newRequest = original.newBuilder()
                            .addHeader("Api-key", BuildConfig.API_KEY)
                            .addHeader("Content-Type", "application/json")
                            .build()

                    it.proceed(newRequest)
                }
        return clientBuilder.build()
    }

    @Provides fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .build()
        return retrofit
    }
}