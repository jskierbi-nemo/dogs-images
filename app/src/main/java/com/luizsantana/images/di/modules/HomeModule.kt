package com.luizsantana.images.di.modules

import com.luizsantana.images.mvp.home.HomeContract
import com.luizsantana.images.services.GettyService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class HomeModule(var homeView: HomeContract.View) {

    @Provides fun providesHomeView(): HomeContract.View {
        return homeView
    }

    @Provides fun provideImageService(retrofit: Retrofit): GettyService {
        return retrofit.create(GettyService::class.java)
    }
}