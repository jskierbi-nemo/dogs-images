package com.luizsantana.images.model

open class Image(
        val id: String,
        val title: String,
        val caption: String,
        val displaySizes: List<Data>
) {

    fun getThumbURL(): String {
        val filteredImages = displaySizes
                .filter { it.name == "thumb" }

        if (filteredImages.isEmpty()) {
            return filteredImages.first().uri
        }

        return filteredImages.first().uri
    }

    data class Data(
            val name: String,
            val uri: String)
}
