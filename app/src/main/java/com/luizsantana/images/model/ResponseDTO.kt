package com.luizsantana.images.model

data class ResponseDTO<T>(
        val resultCount: Int,
        val images: List<T>
)