package com.luizsantana.images.services

import com.luizsantana.images.model.Image
import com.luizsantana.images.model.ResponseDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface GettyService {
    @GET("/v3/search/images")
    fun load(@Query("page") page: Int,
             @Query("page_size") pageSize: Int = 20,
             @Query("phrase") searchTerm: String = "dog"): rx.Observable<ResponseDTO<Image>>
}
