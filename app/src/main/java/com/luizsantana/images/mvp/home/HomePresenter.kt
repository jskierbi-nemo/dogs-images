package com.luizsantana.images.mvp.home

import com.luizsantana.images.model.Image
import com.luizsantana.images.model.ResponseDTO
import com.luizsantana.images.services.GettyService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class HomePresenter @Inject constructor(var homeView: HomeContract.View,
                                        var imagesService: GettyService): HomeContract.Presenter {

    val PAGE_SIZE = 20
    var lastPage = -1

    override fun load(page: Int) {
        if (isInvalidPage(page)) {
            return
        }

        imagesService.load(page = page, pageSize = PAGE_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.resultCount == 0) {
                        homeView.showEmptyResultMessage()
                    }

                    calculateLastPage(it)
                    homeView.addListOfResults(it.images)
                }, {
                    it.printStackTrace()
                    homeView.showError()
                })
    }

    private fun calculateLastPage(it: ResponseDTO<Image>) {
        if (lastPage < 0) {
            lastPage = Math.ceil(
                    it.resultCount.toDouble() / PAGE_SIZE.toDouble()
            ).toInt()
        }
    }

    private fun isInvalidPage(page: Int) = lastPage != -1 && page > lastPage

}