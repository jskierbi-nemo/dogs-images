package com.luizsantana.images.mvp.home

import com.luizsantana.images.model.Image
import com.luizsantana.images.mvp.BasePresenter
import com.luizsantana.images.mvp.BaseView

class HomeContract {

    interface View : BaseView {
        fun showError()
        fun showEmptyResultMessage()
        fun addListOfResults(data: List<Image>)
    }

    interface Presenter : BasePresenter {
        fun load(page: Int = 1)
    }
}