package com.luizsantana.images.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.luizsantana.images.MainApplication
import com.luizsantana.images.R
import com.luizsantana.images.di.components.DaggerHomeComponent
import com.luizsantana.images.di.modules.HomeModule
import com.luizsantana.images.fragments.ImagesListFragment
import com.luizsantana.images.model.Image
import com.luizsantana.images.mvp.home.HomeContract
import com.luizsantana.images.mvp.home.HomePresenter
import com.luizsantana.images.views.listener.InfiniteScrollListener
import javax.inject.Inject

class HomeActivity : AppCompatActivity(), HomeContract.View {

    @Inject
    lateinit var presenter: HomePresenter
    var fragment: ImagesListFragment? = null
    var currentPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupDependencyInjection()
        initializeFragmentInstance()
        show(fragment!!)
        presenter.load(currentPage++)
    }

    private fun setupDependencyInjection() {
        DaggerHomeComponent.builder()
                .applicatonComponent((application as MainApplication).appComponent)
                .homeModule(HomeModule(this))
                .build()
                .inject(this)
    }

    private fun show(fragment: Fragment) {
        if (fragment.isVisible) {
            return
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.homeContainer, fragment)
                .commitAllowingStateLoss()
    }

    override fun showError() {
        AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage("An unexpected error has occurred :(")
                .setNeutralButton("Ok", { dialogInterface, i -> dialogInterface.dismiss() })
                .create()
                .show()
    }


    override fun showEmptyResultMessage() {

    }

    override fun addListOfResults(data: List<Image>) {
        initializeFragmentInstance()
        show(fragment!!)

        fragment!!.addData(data)
    }

    private fun initializeFragmentInstance() {
        if (fragment != null) {
            return
        }
        fragment = ImagesListFragment()
        fragment?.infiniteScrollListener = InfiniteScrollListener({ presenter.load(currentPage++) })
    }
}
