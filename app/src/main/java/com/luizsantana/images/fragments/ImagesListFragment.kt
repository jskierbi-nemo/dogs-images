package com.luizsantana.images.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.luizsantana.images.R
import com.luizsantana.images.extensions.inflate
import com.luizsantana.images.model.Image
import com.luizsantana.images.views.ImagesAdapter
import com.luizsantana.images.views.listener.InfiniteScrollListener
import kotlinx.android.synthetic.main.fragment_images_list.*

class ImagesListFragment : Fragment() {

    private val soundsAdapter = ImagesAdapter()
    var infiniteScrollListener: InfiniteScrollListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_images_list)
    }

    override fun onStart() {
        super.onStart()
        retainInstance = true
        imagesRecyclerView.setHasFixedSize(true)
        imagesRecyclerView.adapter = soundsAdapter
        val layoutManager = LinearLayoutManager(activity)
        imagesRecyclerView.layoutManager = layoutManager
        infiniteScrollListener?.layoutManager = layoutManager
        imagesRecyclerView.addOnScrollListener(infiniteScrollListener)
    }

    fun addData(sounds: List<Image>) {
        var lastIndex = soundsAdapter.dataList.size - 1
        soundsAdapter.dataList.addAll(lastIndex, sounds)
        soundsAdapter.notifyDataSetChanged()
    }
}