package com.luizsantana.images

import android.app.Application
import com.facebook.stetho.Stetho
import com.luizsantana.images.di.components.ApplicatonComponent
import com.luizsantana.images.di.components.DaggerApplicatonComponent
import com.luizsantana.images.di.modules.AndroidModule
import com.luizsantana.images.di.modules.NetworkModule
import timber.log.Timber

class MainApplication : Application() {

    lateinit var appComponent: ApplicatonComponent

    override fun onCreate() {
        super.onCreate()
        initializeDependencyInjection()
        initializeLoggingConfig()
        setupStetho()
    }

    private fun initializeDependencyInjection() {
        appComponent = DaggerApplicatonComponent.builder()
                .androidModule(AndroidModule(applicationContext))
                .networkModule(NetworkModule(this))
                .build()
    }

    private fun setupStetho() {
        Stetho.initializeWithDefaults(this)
    }

    private fun initializeLoggingConfig() {
        Timber.plant(Timber.DebugTree())
    }


    fun getApplicationComponent(): ApplicatonComponent {
        return appComponent
    }

}