package com.luizsantana.images.views

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.luizsantana.images.R
import com.luizsantana.images.extensions.load
import com.luizsantana.images.model.Image

class DogImageDialog(context: Context, val data: Image) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_dog_image)

        bindData()
        setupCloseButton()
    }

    private fun setupCloseButton() {
        findViewById(R.id.dialogOk).setOnClickListener { dismiss() }
    }

    private fun bindData() {
        val image = findViewById(R.id.dialogDogImage) as ImageView
        val caption = findViewById(R.id.dialogCaption) as TextView

        image.load(data.getThumbURL(), circular = true)
        caption.text = data.caption
    }
}