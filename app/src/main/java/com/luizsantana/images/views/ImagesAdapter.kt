package com.luizsantana.images.views

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.luizsantana.images.R
import com.luizsantana.images.extensions.inflate
import com.luizsantana.images.extensions.load
import com.luizsantana.images.model.Image
import java.util.*

class ImagesAdapter(var dataList: MutableList<Image> = ArrayList<Image>())
            : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    val LOADING = 1
    val IMAGE = 2

    init {
        dataList.add(LoadingImage())
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(dataList.get(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        when(viewType) {
            IMAGE -> return ViewHolder(parent?.inflate(R.layout.item_dog_image))
        }

        return LoadingViewHolder(parent?.inflate(R.layout.item_loading))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun getItemViewType(position: Int): Int {
        when(dataList[position].javaClass) {
            Image::class.java -> return IMAGE
            LoadingImage::class.java -> return LOADING
        }

        return -1
    }

    class LoadingViewHolder(var loadingView: View?): ViewHolder(loadingView) {
        override fun bind(data: Image) { }
    }

    open class ViewHolder(var view: View?): RecyclerView.ViewHolder(view) {
        var imageTitle: TextView? = null
        var id: TextView? = null
        var image: ImageView? = null
        var container: View? = null

        init {
            imageTitle = view?.findViewById(R.id.imageTitle) as TextView?
            id = view?.findViewById(R.id.imageId) as TextView?
            image = view?.findViewById(R.id.dogImage) as ImageView?
            container = view?.findViewById(R.id.dogImageContainer)
        }

        open fun bind(data: Image) {
            imageTitle?.text = data.title
            id?.text = data.id.toString()
            image?.load(
                    url = data.getThumbURL(),
                    circular = true)
            container?.setOnClickListener {
                DogImageDialog(container!!.context, data).show()
            }
        }
    }

    class LoadingImage: Image("Not valid", "Not valid", "Not Valid", Collections.emptyList())

}