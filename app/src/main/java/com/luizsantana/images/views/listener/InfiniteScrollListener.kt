package com.luizsantana.images.views.listener

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import timber.log.Timber

class InfiniteScrollListener(val loadFunction: () -> Unit) : RecyclerView.OnScrollListener() {

    private var previousTotal = 0
    private var loading = true
    private var visibleThreshold = 2
    private var firstVisibleItem = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    lateinit var layoutManager: LinearLayoutManager

    override fun onScrolled(recyclerView: RecyclerView?, deltaX: Int, deltaY: Int) {
        super.onScrolled(recyclerView, deltaX, deltaY)

        if (deltaY <= 0) {
            return
        }
        visibleItemCount = recyclerView!!.childCount
        totalItemCount = layoutManager.itemCount
        firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }

        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            Timber.i("End reached")
            loadFunction()
            loading = true
        }
    }
}