package com.luizsantana.images

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.luizsantana.images.activity.HomeActivity
import com.luizsantana.images.utils.TestUtils.withRecyclerView
import com.squareup.spoon.Spoon
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class HomeActivityTests {
    @get:Rule
    var activityTestRule = ActivityTestRule<HomeActivity>(HomeActivity::class.java)

    @Test
    fun showLoadingSpinner() {
         Spoon.screenshot(activityTestRule.activity, "just-open")

        onView(withRecyclerView(R.id.imagesRecyclerView)
                .atPositionOnView(0, R.id.loadingProgress))
                .check(matches(isDisplayed()))

         Spoon.screenshot(activityTestRule.activity, "finished")
    }

    @Test
    fun showLoadingDialogDetails() {
        onView(withRecyclerView(R.id.imagesRecyclerView)
                .atPosition(0))
                .perform(click())

        onView(withId(R.id.dialogDogImage))
                .check(matches(isDisplayed()))
         Spoon.screenshot(activityTestRule.activity, "dialog open")
    }
}