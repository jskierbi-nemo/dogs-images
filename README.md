# Dog Images app
App for list images from Getty API

## Dependencies
* [Kotlin](http://kotlinlang.org) as main language
* [Timber](https://github.com/JakeWharton/timber) for Loggin
* [Dagger 2](https://google.github.io/dagger/) for dependency injection
* [RxJava/RxAndroid](https://github.com/ReactiveX/RxJava/wiki/Plugins) for reactive programming
* [Glide](https://github.com/bumptech/glide) for image loading
* [Spoon](https://github.com/square/spoon) for better test reports
    * run with `./gradlew spoon`
* [Spek](https://jetbrains.github.io/spek/) for more readable test specs
* Mockito for mocking tests
* [Stetho](http://facebook.github.io/stetho/) for debugging. This way we are able to inspect/debug database, views, network, etc...
    * Try running: `chrome://inspect/#devices` in your Chrome browser
